//
//  BudgetCellView.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 09/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class BudgetCellView : UITableViewCell {
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var used: UILabel!
    @IBOutlet weak var rest: UILabel!
}
