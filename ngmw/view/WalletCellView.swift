//
//  WalletCellView.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 06/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class WalletCellView : UITableViewCell {
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var icon: UIImageView!
}