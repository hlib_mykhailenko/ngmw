//
//  TransactionCellView.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 27/07/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class TransactionCellView : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!

}
