//
//  DAOBudget.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 09/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

class DAOBudget {
    
    static let sharedInstance = DAOBudget()
    
    func getBudgetReports() -> [BudgetReport] {
        return [BudgetReport(id: "all", name: "Total Spent", amount: 700, usedAmount: 400, restAmount: 300),
                BudgetReport(id: "all", name: "Food", amount: 400, usedAmount: 100, restAmount: 300),
                BudgetReport(id: "all", name: "Transport", amount: 60, usedAmount: 100, restAmount: -40),
                BudgetReport(id: "all", name: "Clothes", amount: 240, usedAmount: 200, restAmount: 40)]
    }
}