//
//  DAOTransactions.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 07/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

class DAOTransaction {
    static let sharedInstance = DAOTransaction()
    
    func getAll(walletId : String) -> [Transaction] {
        let dateFormatter = NSDateFormatter()

        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"

        let t1 = Transaction(amount: -72, description: "Water", userDate: dateFormatter.dateFromString("06-08-2016 16:34")!, categoryIconName: "water.png")
        let t2 = Transaction(amount: -9, description: "Mac", userDate: dateFormatter.dateFromString("06-08-2016 16:40")!, categoryIconName: "water.png")
        let t3 = Transaction(amount: 1488, description: "Salary", userDate: dateFormatter.dateFromString("06-08-2016 16:40")!, categoryIconName: "water.png")
        let t4 = Transaction(amount: -72, description: "Water", userDate: dateFormatter.dateFromString("07-08-2016 16:34")!, categoryIconName: "water.png")
        let t5 = Transaction(amount: -9, description: "Mac", userDate: dateFormatter.dateFromString("07-08-2016 16:40")!, categoryIconName: "water.png")
        let t6 = Transaction(amount: 1488, description: "Salary", userDate: dateFormatter.dateFromString("07-08-2016 16:40")!, categoryIconName: "water.png")
        return [t1, t2, t3, t4, t5, t6]
    }
}