//
//  DAOWallet.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 06/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

class DAOWallet {
    static let sharedInstance = DAOWallet()
    
    func getAll() -> [Wallet] {
        
        return [Wallet(id: "Sasha", name: "Sasha", currency: Currency.USD, icon: "open_wallet", balance: 100300),
                Wallet(id: "Jenechka", name: "Jenechka", currency: Currency.EURO, icon: "rabbit", balance: 1589),
                Wallet(id: "USD Card_2", name: "USD Card_2", currency: Currency.HRN, icon: "wallet", balance: 1000300),
                Wallet(id: "EURO", name: "EURO", currency: Currency.USD, icon: "wallet", balance: 1488)]
    }
}