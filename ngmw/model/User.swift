//
//  User.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 12/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

class User {
    var login : String?
    var authString : String?
    var wallets : [Wallet]?
    var settings : UserSettings?
    
}