//
//  Wallet.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 12/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

class Wallet {
    var id : String?
    var name : String?
    var currency : Currency?
    var icon : String?
    var balance : Int
    init(id : String, name : String, currency : Currency, icon : String, balance : Int){
        self.id = id
        self.name = name
        self.currency = currency
        self.icon = icon
        self.balance = balance
    }
  
}