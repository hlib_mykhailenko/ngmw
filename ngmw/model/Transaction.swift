//
//  Transaction.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 12/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class Transaction {
    var amount : Int?
    var description : String?
    var userDate : NSDate?
//    var creationDate : NSDate?
    var categoryIconName : String?
    init(amount : Int, description: String, userDate: NSDate, categoryIconName : String){
        self.amount = amount
        self.description = description
        self.userDate = userDate
        self.categoryIconName = categoryIconName
    }
   
}

class TransactionsForDay {
    var transacations: [Transaction]
    var day : NSDate
    init(day : NSDate, transactions: [Transaction]){
        self.day = day
        self.transacations = transactions
    }
}

public extension SequenceType {
    
    /// Categorises elements of self into a dictionary, with the keys given by keyFunc
    
    func categorise<U : Hashable>(@noescape keyFunc: Generator.Element -> U) -> [U:[Generator.Element]] {
        var dict: [U:[Generator.Element]] = [:]
        for el in self {
            let key = keyFunc(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}