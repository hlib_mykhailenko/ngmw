//
//  BudgetReport.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 09/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

class BudgetReport {
    var amount : Int
    var usedAmount : Int
    var restAmount : Int
    var name : String
    var id : String
    var currency : Currency
    init(id: String, name: String, amount: Int, usedAmount : Int, restAmount: Int){
        self.id = id
        self.name = name
        self.amount = amount
        self.usedAmount = usedAmount
        self.restAmount = restAmount
        self.currency = Currency.EURO
    }
}