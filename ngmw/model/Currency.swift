//
//  Currency.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 12/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation

enum Currency : String {
    case USD = "USD"
    case EURO = "EUR"
    case HRN = "UAH"
    
    static func getCurrencySymbol(acode : String) -> String {
        let locales: NSArray = NSLocale.availableLocaleIdentifiers()
        for localeID in locales as! [NSString] {
            let locale = NSLocale(localeIdentifier: localeID as String)
            let code = locale.objectForKey(NSLocaleCurrencyCode) as? String
            if code == acode {
                let symbol = locale.objectForKey(NSLocaleCurrencySymbol) as? String
                return symbol!
            }
        }
        return acode
    }
    
}