//
//  Category.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 12/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import Foundation
import UIKit

class Category {
    var id : String?
    var icon : UIImage?
}