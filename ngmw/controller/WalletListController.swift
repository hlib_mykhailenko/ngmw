//
//  WalletListController.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 06/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit


class WalletListController : UIViewController , UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var wallets: UITableView!
    
    var dataWallets = DAOWallet.sharedInstance.getAll()

    override func viewDidLoad() {
        self.wallets.delegate = self
        self.wallets.dataSource = self
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataWallets.count + 1
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section \(section)"
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row < dataWallets.count){
            self.performSegueWithIdentifier("wallet", sender: self)
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row < dataWallets.count){
            let cell = tableView.dequeueReusableCellWithIdentifier("wallet", forIndexPath: indexPath)
               as! WalletCellView
            let wallet = dataWallets[indexPath.row]
            cell.title.text = wallet.name
            cell.amount.text = Currency.getCurrencySymbol(wallet.currency!.rawValue) + String(wallet.balance)
            cell.whiteView.layer.borderWidth = 1
            cell.whiteView.layer.borderColor = UIColor(red: 225/256, green: 225/256, blue: 225/256, alpha: 1.0).CGColor
            
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("button", forIndexPath: indexPath) as! AddWalletCellView
            cell.addWalletButton.layer.cornerRadius = 10
            
            //cell.titleLabel?.text = "Water \(indexPath.row)"
            
            return cell
        }
        
    }
    
    
    
    
}