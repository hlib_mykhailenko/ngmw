//
//  ViewController.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 21/05/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTestField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   // MAIN CONTROLLER SHOULD BE HERE
    
 
    
    @IBAction func createTransaction(sender: UIButton) {
        
        self.performSegueWithIdentifier("showMainMenu", sender: self)
    }
    
    @IBAction func numKeyboard(sender: UIButton) {
        self.performSegueWithIdentifier("numkeyboard", sender: self)
    }
    @IBOutlet weak var amountLabel: UILabel!
   
    @IBAction func zero(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "0"
    }
    @IBAction func one(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "1"
    }
    @IBAction func two(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "2"
    }
    @IBAction func three(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "3"
    }
    @IBAction func four(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "4"
    }
    @IBAction func five(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "5"
    }
    @IBAction func six(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "6"
    }
    @IBAction func seven(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "7"
    }
    @IBAction func eight(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "8"
    }
    @IBAction func nine(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "9"
    }
    @IBAction func minus(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "-"
    }
    @IBAction func plus(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "+"
    }
    @IBAction func backspace(sender: UIButton) {
        amountLabel.text = "aint working yet"
    }
    @IBAction func point(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "."
    }
    @IBAction func equal(sender: UIButton) {
        amountLabel.text = (amountLabel?.text)! + "why do you even need equal here"
    }
    @IBAction func clear(sender: UIButton) {
        amountLabel.text = ""

    }
    @IBAction func completeAmount(sender: UIButton) {
        self.performSegueWithIdentifier("paymentMenu", sender: self)

    }
    @IBAction func back(sender: UIButton) {
        self.performSegueWithIdentifier("paymentMenu", sender: self)
    }
}

