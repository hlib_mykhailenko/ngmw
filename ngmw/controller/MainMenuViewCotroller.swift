//
//  MainMenuViewCotroller.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 04/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class MainMenuViewController : UIViewController {
    
    @IBOutlet weak var walletsButton: UIButton!
    @IBOutlet weak var budgetButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var newNoteButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        walletsButton.layer.cornerRadius = 10
        budgetButton.layer.cornerRadius = 10
        reportButton.layer.cornerRadius = 10
        newNoteButton.layer.cornerRadius = 10
        
    }
}
