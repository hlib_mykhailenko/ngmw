//
//  KeyboardViewController.swift
//  ngmw
//
//  Created by Hlib Mykhailenko on 03/06/16.
//  Copyright © 2016 Hlib Mykhailenko. All rights reserved.
//

import UIKit

class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?)-> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView();
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
    //    let screens : (from:UIViewController, to:UIViewController) = (transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!, transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!)
        fromViewController?.definesPresentationContext = true
        toViewController!.modalPresentationStyle = UIModalPresentationStyle.Custom;


//        toViewController?.beginAppearanceTransition(true, animated: true)
        
        UIView.animateWithDuration(1, animations: {
            containerView?.addSubview(fromViewController!.view)
            
            containerView?.addSubview(toViewController!.view)
            containerView?.bringSubviewToFront(toViewController!.view)

            fromViewController!.view.frame = CGRectMake(0,0,375, 600)
            toViewController!.view.frame = CGRectMake(0,136,375,531)
            fromViewController!.view.alpha = 1
            fromViewController!.view.hidden = false
            


        }) { (finished) in
    //        toViewController?.endAppearanceTransition()
            transitionContext.completeTransition(finished)
      //      UIApplication.sharedApplication().keyWindow?.addSubview(screens.from.view)

        }
        
        
    }
    
}