//
//  LoginViewController.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 29/05/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginTextField: UITextField!

    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loginTextField.layer.cornerRadius = 10
        passwordTextField.layer.cornerRadius = 10
        loginButton.layer.cornerRadius = 10
        
        loginTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    
    @IBAction func login(sender: UIButton) {
        //self.performSegueWithIdentifier("showMainMenu", sender: self)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == loginTextField){
            passwordTextField.becomeFirstResponder()
        }else{
            passwordTextField.resignFirstResponder()
        }
        return false
    }
}