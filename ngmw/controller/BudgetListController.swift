//
//  BudgetListController.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 09/08/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit

class BudgetListController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var budgetTable: UITableView!
    
    var budgets = DAOBudget.sharedInstance.getBudgetReports()
    
    override func viewDidLoad() {
        self.budgetTable.delegate = self
        self.budgetTable.dataSource = self
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return budgets.count + 1
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row < budgets.count){
            let cell = tableView.dequeueReusableCellWithIdentifier("budget", forIndexPath: indexPath) as! BudgetCellView
            let budget = budgets[indexPath.row]
            cell.title?.text = budget.name
            cell.total?.text = Currency.getCurrencySymbol(budget.currency.rawValue) +  String(budget.amount)
            cell.used?.text = Currency.getCurrencySymbol(budget.currency.rawValue) +  String(budget.usedAmount)
            cell.rest?.text = Currency.getCurrencySymbol(budget.currency.rawValue) +  String(budget.restAmount)
            cell.whiteView?.layer.borderWidth = 0.5
            
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = (cell.gradientView?.bounds)!
            gradient.colors = [UIColor.greenColor().CGColor, UIColor.redColor().CGColor]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            cell.gradientView?.layer.insertSublayer(gradient, atIndex: 0)
            var r = Float(budget.restAmount) / Float(budget.amount)
            if(r < 0){
                r = 0.0
            }
            
            var greyView = UIView(frame: CGRectMake(CGFloat(1-r)*gradient.frame.width, 0, gradient.frame.width * CGFloat(r), gradient.frame.height))
            greyView.backgroundColor = UIColor.grayColor()
            cell.gradientView?.addSubview(greyView)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("button", forIndexPath: indexPath)
            return cell
        }
    }
}
