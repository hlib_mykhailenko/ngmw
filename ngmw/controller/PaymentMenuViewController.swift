//
//  MainMenuViewController.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 29/05/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit


class PaymentMenuViewController : UIViewController, UIViewControllerTransitioningDelegate {
  
    
    let transition = PopAnimator()

    
    @IBOutlet var categories: [UIButton]!
    @IBOutlet var wallets: [UIButton]!
    
    
    
    func animationControllerForPresentedController(
        presented: UIViewController,
        presentingController presenting: UIViewController,
                             sourceController source: UIViewController) ->
        UIViewControllerAnimatedTransitioning? {
            return transition
    }
    
   
    
    
    @IBAction func showKeyboard(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("keyboard") as! KeyboardViewController
        
        vc.transitioningDelegate = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var backToMenu: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        optionsButton.layer.cornerRadius = 10
    }

    @IBAction func selected(sender: UIButton) {
        sender.selected = !sender.selected
        if(sender.selected && wallets.contains(sender)){
            unselectAllExcept(wallets, exception: sender)
        }
    }

    func unselectAllExcept(buttons : [UIButton], exception : UIButton) -> Void {
        for button in buttons {
            if(button != exception){
                button.selected = false
            }
        }
        
    }
    
}
