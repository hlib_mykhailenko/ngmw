//
//  WalletHistoryTableViewController.swift
//  ngmw
//
//  Created by Oleksandra Kulankhina on 14/06/16.
//  Copyright © 2016 Oleksandra Kulankhina. All rights reserved.
//

import UIKit



class WalletHistoryTableViewController : UIViewController , UITableViewDelegate, UITableViewDataSource {
    

  
    @IBOutlet weak var transactionTable: UITableView!

    
    
    var transactions = DAOTransaction.sharedInstance.getAll("Sasha")
    
    var arrayTFD = [TransactionsForDay]()

    override func viewDidLoad() {
       self.transactionTable.delegate = self
       self.transactionTable.dataSource = self
     
        let sorted = transactions.sort({ $0.userDate!.compare($1.userDate!) == NSComparisonResult.OrderedAscending })

        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        
        var anArray = [Transaction]()
        var currDate = dateFormatter.stringFromDate(sorted[0].userDate!)
        for t in sorted {
            let ts = dateFormatter.stringFromDate(t.userDate!)
            if(ts == currDate){
                anArray.append(t)
            }else{
                arrayTFD.append(TransactionsForDay(day: anArray[0].userDate!, transactions: anArray))
                anArray = [t]
                currDate = dateFormatter.stringFromDate(t.userDate!)
            }
        }
        arrayTFD.append(TransactionsForDay(day: anArray[0].userDate!, transactions: anArray))
    }
    
    func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
        return arrayTFD.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTFD[section].transacations.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMMM"
        return dateFormatter.stringFromDate( arrayTFD[section].day )
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("transaction", forIndexPath: indexPath) as!TransactionCellView
        
        let t = arrayTFD[indexPath.section].transacations[indexPath.row]
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        cell.titleLabel?.text = t.description
        var sign = "+"
        cell.amountLabel?.textColor = UIColor.greenColor()
        if (t.amount < 0){
            sign = ""
            cell.amountLabel?.textColor = UIColor.redColor()
        }
        cell.amountLabel?.text = sign + String(t.amount!)
        cell.timeLabel?.text = dateFormatter.stringFromDate(t.userDate!)
        
        return cell
    }

    
    
    
}

